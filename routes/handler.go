package routes

import (
	"html/template"
	"net/http"

	"github.com/go-martini/martini"
)

func LoginHandler(w http.ResponseWriter, b AssetBit) {
	base := b.Assets.MustString("login.html")
	temp, _ := template.New("login").Funcs(b.Func).Parse(base)
	temp.ExecuteTemplate(w, "login", nil)
}

func IndexHandler(params martini.Params, w http.ResponseWriter, b AssetBit) { //DB *gorm.DB,
	// var limit = 10
	// var pageNum int
	// var contact []db.Contact
	// var count int64
	// DB.Find(&contact).Count(&count)

	// if params["id"] == "" || params["id"] == "1" {
	// 	pageNum = 1
	// 	DB.Order("id desc").Offset(pageNum - 1).Limit(limit).Find(&contact)
	// } else {
	// 	pageNum, _ = strconv.Atoi(params["id"])
	// 	DB.Order("id desc").Offset(limit * (pageNum - 1)).Limit(limit).Find(&contact)
	// }
	// model := db.ModelContact{}
	// model.Posts = contact
	// model.Paginate = tools.Pages(count, limit, pageNum)

	header := b.Assets.MustString("header.html")
	footer := b.Assets.MustString("footer.html")
	base := b.Assets.MustString("index.html")
	temp, _ := template.New("index").Funcs(b.Func).Parse(header + footer + base)
	temp.ExecuteTemplate(w, "header", "index")
	// temp.ExecuteTemplate(w, "index", &model)
	//render.HTML(200, "index", &model)
}
