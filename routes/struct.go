package routes

import (
	"html/template"

	rice "github.com/GeertJohan/go.rice"
)

type AssetBit struct {
	Assets *rice.Box
	Func   template.FuncMap
}
