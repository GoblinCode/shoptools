package tools

import (
	"bytes"
	"log"
	"net/http"
	"time"

	rice "github.com/GeertJohan/go.rice"
	"github.com/go-martini/martini"
)

func Static(path string, box *rice.Box) martini.Handler {

	modtime := time.Now()

	return func(res http.ResponseWriter, req *http.Request, log *log.Logger) {
		if req.Method != "GET" && req.Method != "HEAD" {
			return
		}

		url := req.URL.Path

		b, err := box.HTTPBox().Bytes(path + url)

		if err != nil {
			// Try to serve the index file.
			return

		}
		log.Println("[Static] Serving " + url)

		http.ServeContent(res, req, url, modtime, bytes.NewReader(b))
	}
}
