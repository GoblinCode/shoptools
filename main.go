package main

import (
	"bitbucket.org/GoblinCode/shoptools/routes"
	"bitbucket.org/GoblinCode/shoptools/tools"

	rice "github.com/GeertJohan/go.rice"
	"github.com/go-martini/martini"
)

func main() {
	assets, err := rice.FindBox("template")
	if err != nil {
		println(err)
	}
	var Box = routes.AssetBit{
		Assets: assets,
	}
	m := martini.Classic()
	m.Use(tools.Static("assets", Box.Assets))
	m.Map(Box)
	m.Get("/", routes.IndexHandler)
	m.Get("/login", routes.LoginHandler)
	m.Run()
	// rice embed-go && go run ./

}
